#include <cctype>
#include <iostream>

#include "StackLi.h"
#include "LinkedList.h"
#include "LongInt.h"

LongInt::LongInt() : size(0) 
{
} // LongInt()

LongInt LongInt::operator+ (LongInt &lI)
{
  LongInt newLI;

  int remainder = 0, extra = 0, i = 0, sum = 0, size = 0;  

  while(!lI.stack.isEmpty() && !stack.isEmpty())
  {   
    sum = lI.stack.topAndPop() + stack.topAndPop() + extra;
    extra = 0;
    remainder = sum;
         
    if(sum > 9)
    {   
      remainder = sum - 10; 
      extra = 1;
    } // if sum is greater than 9 / 10 or higher

    newLI.stack.push(remainder);
    newLI.size++;
  } // while there are numbers in each stack

  newLI.extraNumbers(*this, lI, extra);
  return newLI;
} // operator+

void LongInt::extraNumbers(LongInt &lI, LongInt &lI2, int extra)
{
  int remainder = 0;

  if(lI.size == lI2.size && extra)
  {
    stack.push(1);
    return;
  } // if the sizes of both integers are the same and carrying the one

  if(lI.size > lI2.size) // check for bigger number
    while(!lI.stack.isEmpty())
    {
      remainder = lI.stack.topAndPop() + extra;
      stack.push(remainder);
      extra = 0;
    } // while there are still elements in list1

  else // else it is the other number
    while(!lI2.stack.isEmpty())
    {
      remainder = lI2.stack.topAndPop() + extra;
      stack.push(remainder);
      extra = 0;
    } // while there are still  elements in list2

} // extraNumbers()

istream& operator>> (istream &is, LongInt &lI)
{
  char buffer;

  while(isdigit(is.peek()))
  {   
    is >> buffer;

    lI.stack.push(buffer - '0');
    lI.size++;
  } // while going through the buffer
  
  is.ignore(10, '\n');

  return is; 
} // operator>>

ostream& operator<< (ostream &os, LongInt &lI)
{
  while(!lI.stack.isEmpty())
    os << lI.stack.topAndPop();

  return os; 
} // operator<<
