#include <iostream>
#include <fstream>

#include "CPUTimer.h"
#include "CursorList.h"
#include "LinkedList.h"
#include "StackAr.h"
#include "StackLi.h"
#include "QueueAr.h"
#include "SkipList.h"

vector<CursorNode <int> > cursorSpace(2500001);

int getChoice();
void RunList(const char f[]);
void RunCursorList(const char f[]);
void RunStackAr(const char f[]); 
void RunStackLi(const char f[]);
void RunQueueAr(const char f[]); 
void RunSkipList(const char f[]); 

int main()
{
  CPUTimer ct;
  int choice = 0;
  char filename[80]; 
 
  cout << "Filename >> ";
  cin >> filename;

  do
  {
    choice = getChoice();
    ct.reset();

    switch (choice)
    {
      case 1: RunList(filename); break;
      case 2: RunCursorList(filename); break;
      case 3: RunStackAr(filename); break;
      case 4: RunStackLi(filename); break;
      case 5: RunQueueAr(filename); break;
      case 6: RunSkipList(filename); break;
    }

    cout << "CPU time: " << ct.cur_CPUTime() << endl;
  } while(choice > 0);

  return 0;
}

int getChoice()
{
  int choice = 0;

  cout << endl << "      ADT Menu" << endl;
  cout << "0. Quit" << endl;
  cout << "1. LinkedList" << endl;
  cout << "2. CursorList" << endl;
  cout << "3. StackAr" << endl;
  cout << "4. StackLi" << endl;
  cout << "5. QueueAr" << endl;
  cout << "6. SkipList" << endl;

  cout << "Your choice >> ";
  cin >> choice;

  return choice;
} // getChoice()


void RunList(const char f[])
{
  List<int> list;
  ListItr<int> itr = list.zeroth();
  char command;
  int num;

  ifstream inf(f);
  inf.ignore(1000, '\n');
  
  while(inf >> command >> num)
  {
    if(command == 'i')
      list.insert(num, itr);
    else
      list.remove(num);
  }

  inf.close();
} // RunList()

void RunCursorList(const char f[])
{
  CursorList<int> cursorList(cursorSpace);
  CursorListItr<int> cLItr = cursorList.zeroth();
  char command;
  int num, index = 0;

  ifstream inf(f);

  inf.ignore(1000, '\n');
  
  while(inf >> command >> num)
  {
    if(command == 'i')
      cursorList.insert(num, cLItr);
    else
      cursorList.remove(num);
  }

  inf.close();
} // RunCursorlist()

void RunStackAr(const char f[])
{
  StackAr<int> stack(2500001);
  char command;
  int num, index = 0;

  ifstream inf(f);
  inf.ignore(1000, '\n');
  
  while(inf >> command >> num)
  {
    if(command == 'i')
      stack.push(num);
    else
      stack.pop();
  }

  inf.close();
} // RunStackAr()

void RunStackLi(const char f[])
{
  StackLi<int> stack;
  char command;
  int num, index = 0;

  ifstream inf(f);
  inf.ignore(1000, '\n');
  
  while(inf >> command >> num)
  {
    if(command == 'i')
      stack.push(num);
    else
      stack.pop();
  }

  inf.close();
} // RunStackLi

void RunQueueAr(const char f[])
{
  Queue<int> queue(2500001);
  char command;
  int num, index = 0;

  ifstream inf(f);
  inf.ignore(1000, '\n');
  
  while(inf >> command >> num)
  {
    if(command == 'i')
      queue.enqueue(num);
    else
      queue.dequeue();
  }

  inf.close();
} // RunQueueAr()

void RunSkipList(const char f[])
{
  SkipList<int> skipList(-1, 2500001);
  char command;
  int num, index = 0;

  ifstream inf(f);
  inf.ignore(1000, '\n');
  
  while(inf >> command >> num)
  {
    if(command == 'i')
      skipList.insert(num);
    else
      skipList.deleteNode(num);
  }

  inf.close();
} // RunSkipList()
