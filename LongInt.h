#ifndef LONGINT_H
#define LONGINT

#include <cctype>
#include <iostream>
#include "StackLi.h"
#include "LinkedList.h"

class LongInt
{
  int size;
  StackLi<int> stack;
public:
  LongInt();
  void extraNumbers(LongInt &lI, LongInt &lI2, int extra);
  friend istream& operator>> (istream &is, LongInt &lI);
  friend ostream& operator<< (ostream &os, LongInt &lI);
  LongInt operator+ (LongInt &lI);
}; // class LongInt


#endif //LONGINT_H
